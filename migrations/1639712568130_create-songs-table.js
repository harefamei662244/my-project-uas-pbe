/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = pgm => {
    pgm.createTable('songs', {
        songs_id: {
            type: 'VARCHAR(50)',
            primaryKey: true
        },
        title: {
            type: 'TEXT',
            notNull: true
        },
        year: {
            type: 'DATE',
            notNull: true
        },
        artist: {
            type: 'TEXT',
            notNull: true
        },
        gendre: {
            type: 'TEXT',
            notNull: true
        },
        duration: {
            type: 'TEXT',
            notNull: true
        },
        created_at: {
            type: 'TEXT',
            notNull: true
        },
        updated_at: {
            type: 'TEXT',
            notNull: true
        },
    });
};

exports.down = pgm => {
    pgm.dropTable('songs');
};
