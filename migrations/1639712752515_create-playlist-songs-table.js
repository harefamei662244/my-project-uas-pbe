/* eslint-disable camelcase */

const {addConstraint} = require("node-pg-migrate/dist/operations/tables");
exports.shorthands = undefined;

exports.up = pgm => {
    pgm.createTable('playlist_songs', {
        id: {
            type: 'INTEGER',
            primaryKey: true,
            autoIncrement: true
        },
        playlist_id: {
            type: 'VARCHAR(50)',
            notNull: true,
            constraint: 'fk_playlist_id',
            foreignKeys: true,
            references: 'playlists(playlists_id)'
        },
        song_id: {
            type: 'VARCHAR(50)',
            notNull: true,
            foreignKeys: true,
            references: 'songs(songs_id)'
        },
        created_at: {
            type: 'TEXT',
            notNull: true
        },
        updated_at: {
            type: 'TEXT',
            notNull: true
        },
    });
};

exports.down = pgm => {
    pgm.dropTable('playlist_songs')
};
