class SongsHandler {
    constructor(service) {
        this._service = service;

        this.addSongHandler = this.addSongHandler.bind(this);
        this.getAllSongHandler = this.getAllSongHandler.bind(this);
        this.getSongByIdHandler = this.getSongByIdHandler.bind(this);
        this.editSongByIdHandler = this.editSongByIdHandler.bind(this);
        this.deleteSongByIdHandler = this.deleteSongByIdHandler.bind(this);
    }

    async addSongHandler(request, h) {
        try {
            const {title, year, artist, gendre, duration} = request.payload;
            const songs_id = await this._service.addSong({title, year, artist, gendre, duration});
            // set response
            const response = h.response({
                status: 'Success!',
                message: 'Lagu berhasil disimpan!',
                data: {
                    songs_id: songs_id
                }
            });
            response.code(201);
            return response;
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }

    async getAllSongHandler() {
        const songs = await this._service.getSongs();
        return {
            status: 'Success!',
            data: {
                songs
            }
        };
    }

    async getSongByIdHandler(request, h) {
        try {
            const {id} = request.params;
            const song = await this._service.getSongById(id);
            return {
                status: 'success!',
                message: 'Lagu ditemukan!',
                data: {
                    song
                }
            }
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }

    async editSongByIdHandler(request, h) {
        try {
            const {id} = request.params;
            const {title, year, artist, gendre, duration} = request.payload;
            const song = await this._service.editSongById(id, {title, year, artist, gendre, duration});
            return {
                status: 'success!',
                message: 'Lagu berhasil diperbaharui!',
                data: {
                    song
                }
            }
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }

    async deleteSongByIdHandler(request, h) {
        try {
            const {id} = request.params;
            await this._service.deleteSongById(id);
            return {
                status: 'success!',
                message: 'Lagu berhasil dihapus!',
            }
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }
}

module.exports = SongsHandler;
