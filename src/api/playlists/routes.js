const routes = (handler) => [
    {
        method: 'POST',
        path: '/playlists',
        handler: handler.addPlaylistHandler,
    },
    {
        method: 'GET',
        path: '/playlists',
        handler: handler.getAllPlaylistHandler,
    },
    {
        method: 'GET',
        path: '/playlists/{id}',
        handler: handler.getPlaylistByIdHandler,
    },
    {
        method: 'PUT',
        path: '/playlists/{id}',
        handler: handler.editPlaylistByIdHandler,
    },
    {
        method: 'DELETE',
        path: '/playlists/{id}',
        handler: handler.deletePlaylistByIdHandler,
    },
];

module.exports = routes;