class PlaylistsHandler {
    constructor(service) {
        this._service = service;

        this.addPlaylistHandler = this.addPlaylistHandler.bind(this);
        this.getAllPlaylistHandler = this.getAllPlaylistHandler.bind(this);
        this.getPlaylistByIdHandler = this.getPlaylistByIdHandler.bind(this);
        this.editPlaylistByIdHandler = this.editPlaylistByIdHandler.bind(this);
        this.deletePlaylistByIdHandler = this.deletePlaylistByIdHandler.bind(this);
    }

    async addPlaylistHandler(request, h) {
        try {
            const {name} = request.payload;
            const playlistId = await this._service.addPlaylist({name});
            // set response
            const response = h.response({
                status: 'Success!',
                message: 'Playlist berhasil disimpan!',
                data: {
                    playlist_id: playlistId
                }
            });
            response.code(201);
            return response;
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }

    async getAllPlaylistHandler() {
        const playlists = await this._service.getPlaylists();
        return {
            status: 'Success!',
            data: {
                playlists
            }
        };
    }

    async getPlaylistByIdHandler(request, h) {
        try {
            const {id} = request.params;
            const playlist = await this._service.getPlaylistById(id);
            return {
                status: 'success!',
                message: 'Lagu ditemukan!',
                data: {
                    playlist
                }
            }
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }

    async editPlaylistByIdHandler(request, h) {
        try {
            const {id} = request.params;
            const {name} = request.payload;
            const playlist = await this._service.editPlaylist(id, {name});
            return {
                status: 'success!',
                message: 'Lagu berhasil diperbaharui!',
                data: {
                    playlist
                }
            }
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }

    async deletePlaylistByIdHandler(request, h) {
        try {
            const {id} = request.params;
            await this._service.deletePlaylistById(id);
            return {
                status: 'success!',
                message: 'Lagu berhasil dihapus!',
            }
        } catch (e) {
            const response = h.response({
                status: 'Failed!',
                message: e.message,
            });
            response.code(400);
            return response;
        }
    }
}

module.exports = PlaylistsHandler;
