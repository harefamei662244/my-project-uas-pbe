// Mengambil database
const {Pool} = require('pg');
const {nanoid} = require('nanoid');

class SongsService {
    constructor() {
        this._pool = new Pool();
    }

    async addSong({title, year, artist, gendre, duration}) {
        const songs_id = nanoid(12);
        const created_at = new Date().toISOString();

        const query = {
            text: 'INSERT INTO songs VALUES($1,$2,$3,$4,$5,$6,$7,$8) RETURNING songs_id',
            values: [songs_id, title, year, artist, gendre, duration, created_at, created_at]
        }

        const result = await this._pool.query(query);
        console.log(result);
        if (!result.rows[0].songs_id) {
            throw new Error("Lagu gagal ditambahkan!");
        }
        return result.rows[0].songs_id;
    }

    async getSongs() {
        const query = 'SELECT * FROM songs';
        const result = await this._pool.query(query);
        return result.rows;
    }

    async getSongById(id) {
        const query = {
            text: 'SELECT * FROM songs WHERE songs_id = $1',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error("Lagu tidak ditemukan, id lagu salah!");
        }
        return result.rows[0];
    }

    async editSongById(id, {title, year, artist, gendre, duration}) {
        const update_at = new Date().toISOString();
        const query = {
            text: 'UPDATE songs SET title=$1,year=$2,artist=$3,gendre=$4,duration=$5,updated_at=$6 WHERE songs_id = $7 RETURNING songs_id',
            values: [title, year, artist, gendre, duration, update_at, id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error("Gagal memperbaharui lagu! Lagu tidak ditemukan!");
        }
        return result.rows[0].songs_id;
    }

    async deleteSongById(id) {
        const query = {
            text: 'DELETE FROM songs WHERE songs_id = $1 RETURNING songs_id',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error("Gagal menghapus lagu! Lagu tidak ditemukan!");
        }
        return result.rows[0].songs_id;
    }
}

module.exports = SongsService;