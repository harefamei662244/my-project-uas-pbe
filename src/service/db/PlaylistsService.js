// Mengambil database
const {Pool} = require('pg');
const {nanoid} = require("nanoid");

class PlaylistsService {
    constructor() {
        this._pool = new Pool();
    }

    async addPlaylist({name}) {
        const playlists_id = nanoid(12);
        const created_at = new Date().toISOString();

        const query = {
            text: 'INSERT INTO playlists VALUES($1,$2,$3,$4) RETURNING playlists_id',
            values: [playlists_id, name, created_at, created_at]
        }

        const result = await this._pool.query(query);

        if (result.rows[0].songs_id) {
            throw new Error("Playlist gagal ditambahkan!");
        }
        return result.rows[0].playlists_id;
    }

    async getPlaylists() {
        const query = 'SELECT * FROM playlists';
        const result = await this._pool.query(query);
        return result.rows;
    }

    async getPlaylistById(id) {
        const query = {
            text: 'SELECT * FROM playlists WHERE playlists_id = $1',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error("Playlist tidak ditemukan!");
        }
        return result.rows[0];
    }

    async editPlaylist(id, {name}) {
        const update_at = new Date().toISOString();
        const query = {
            text: 'UPDATE playlists SET name=$1,updated_at=$2 WHERE playlists_id = $3 RETURNING playlists_id',
            values: [name, update_at, id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error("Gagal memperbaharui playlist! playlist tidak ditemukan!");
        }
        return result.rows[0].playlists_id;
    }

    async deletePlaylistById(id) {
        const query = {
            text: 'DELETE FROM playlists WHERE playlists_id = $1 RETURNING playlists_id',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error("Gagal menghapus lagu! Lagu tidak ditemukan!");
        }
        return result.rows[0].playlists_id;
    }
}

module.exports = PlaylistsService;