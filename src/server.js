require('dotenv').config();

const Hapi = require('@hapi/hapi');

//Songs
const songs = require('./api/songs');
const SongService = require('./service/db/SongsService');

//Playlists
const playlists = require('./api/playlists');
const PlaylistService = require('./service/db/PlaylistsService');

const init = async () => {
    const songsService = new SongService();

    const playlistsService = new PlaylistService();
    const server = Hapi.server({
        port: process.env.PORT,
        host: process.env.HOST,
        routes: {
            cors: {
                origin: ['*']
            }
        }
    });

    //routes and plugin
    await server.register([
        // Plugin Songs
        {
            plugin: songs,
            options: {
                service: songsService,
            }
        },

        // Plugin Playlists
        {
            plugin: playlists,
            options: {
                service: playlistsService,
            }
        }
    ]);
    await server.start();
    console.log(`Server sedang berjalan di ${server.info.uri}`);
};

init();